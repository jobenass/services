#!/bin/sh

# Configuration Nginx
mkdir -p /run/nginx/
mkdir -p /etc/nginx/ssl/
mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.origin

# Extra
mv /tmp/index.html /var/www/localhost/htdocs/
mv /tmp/style.css /var/www/localhost/htdocs/

# Configuration OpenSSH
ssh-keygen -A
adduser --disabled-password --no-create-home --home ./ $USERNAME
echo "$USERNAME:$USERNAME" | chpasswd

# Lancement des services
/usr/sbin/sshd
/usr/sbin/nginx -g "daemon off;"