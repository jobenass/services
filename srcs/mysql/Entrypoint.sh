#!/bin/sh

# Configuration Mysql
mkdir -p /run/mysqld/
mkdir -p /var/log/mysqld/
chown -R mysql:mysql /var/log/mysqld/
/usr/bin/mysql_install_db

# Lancement des services
if [ -d "/data/wordpress" ]; then
	/usr/bin/mysqld_safe
else 
	/usr/bin/mysqld_safe --init-file=/tmp/wordpress.sql
fi
