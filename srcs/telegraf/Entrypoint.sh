#!/bin/sh

# Configuration Telegraf
mkdir -p /etc/telegraf/
mkdir -p /var/log/telegraf/
mv /etc/telegraf.conf /etc/telegraf/telegraf.conf.origin

# Lancement des services
/usr/bin/telegraf