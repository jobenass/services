os=$(uname)
if [ $os = "Linux" ]
then
	priv="sudo"
	$priv mv /home/user42/.kube /home/user42/.minikube $HOME
	$priv chown -R $USER $HOME/.kube $HOME/.minikube
else
	priv=""
fi
objectsList="deployment services persistentvolumeclaim persistentvolume pod secret configmap"
for object in $objectsList
do
	$priv kubectl delete --all $object
done
if [ $os = "Darwin" ]
then
	rm -rf FileZilla*
	eval $(minikube docker-env)
else
	$priv chmod +x /usr/bin/docker-credential-secretservice
fi
echo "y" | $priv docker system prune -a -f
unset os priv object objectsList
