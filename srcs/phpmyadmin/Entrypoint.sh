#!/bin/sh

# Configuration Nginx & Php
mkdir -p /run/nginx/
mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.origin

# Installation Phpmyadmin
tar -xf phpMyAdmin-4.9.5-all-languages.tar.gz
rm -rf phpMyAdmin-4.9.5-all-languages.tar.gz
mv phpMyAdmin-4.9.5-all-languages phpmyadmin
mv /phpmyadmin/* /var/www/localhost/htdocs/
rm -rf phpmyadmin/

# Lancement des services
/usr/sbin/php-fpm7
/usr/sbin/nginx -g "daemon off;"