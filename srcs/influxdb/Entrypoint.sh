#!/bin/sh

# Configuration InfluxDB
mkdir -p /etc/influxdb
mv /etc/influxdb.conf /etc/influxdb/influxdb.conf
export INFLUXDB_CONFIG_PATH=/etc/influxdb/influxdb.conf

# Lancement des services
/usr/sbin/influxd