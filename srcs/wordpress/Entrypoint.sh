#!/bin/sh

# Configuration Nginx & Php
mkdir -p /run/nginx/
mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.origin

# Installation Wordpress
tar -xf latest.tar.gz
rm -rf latest.tar.gz
mv /wordpress/* /var/www/localhost/htdocs/
rm -rf /wordpress

# Lancement des services
/usr/sbin/php-fpm7
/usr/sbin/nginx -g "daemon off;"