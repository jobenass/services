#!/bin/sh

# Configuration Grafana
mkdir -p /etc/grafana/
rm -rf /usr/share/grafana/public/dashboards/*.json
mv /tmp/*.json /usr/share/grafana/public/dashboards/
mv /etc/grafana.ini /etc/grafana/grafana.ini
export GF_PATHS_CONFIG=/etc/grafana/grafana.ini
export GF_PATHS_HOME=/usr/share/grafana/
export GF_DASHBOARDS_DEFAULT_HOME_DASHBOARD_PATH=/usr/share/grafana/public/dashboards/minikube.json
export GF_SECURITY_ADMIN_USER=admin
export GF_SECURITY_ADMIN_PASSWORD=root

# Lancement des services
/usr/sbin/grafana-server --homepath=/usr/share/grafana/