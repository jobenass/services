#!/bin/sh

# Configuration Vsftpd
mkdir -p /etc/vsftpd/ssl/
mkdir -p /var/log/vsftpd/
chmod 755 /home/
echo "root:root" | chpasswd
echo "root" >> /etc/vsftpd/users.list
adduser --disabled-password $USERNAME
echo "$USERNAME:$USERNAME" | chpasswd
touch /home/$USERNAME/empty_file.txt

# Lancement des services
/usr/sbin/vsftpd /etc/vsftpd/vsftpd.conf