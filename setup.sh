os=$(uname)
if [ $os = "Darwin" ]
then
	priv=""
	if [ ! $(command -v brew) ]
	then
		/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
	fi
	brew update
	if [ ! $(command -v virtualbox) ]
	then
		wget https://download.virtualbox.org/virtualbox/6.1.14/VirtualBox-6.1.14-140239-OSX.dmg
		sudo hdiutil attach ./VirtualBox*.dmg
		sudo installer -pkg /Volumes/VirtualBox/VirtualBox.pkg -target /Volumes/Macintosh\ HD
		sudo hdiutil detach /Volumes/VirtualBox/
	fi
	mandatory="minikube kubectl openssl lftp"
	for prog in $mandatory
	do
		if [ ! $(command -v $prog) ]
		then
			brew install $prog
		fi
		if [ $prog = "minikube" ]
		then
			minikube delete
		fi
	done
	chown -R $USER $HOME/.minikube
	chmod -R u+wrx $HOME/.minikube
	minikube start --vm-driver=virtualbox
	minikube addons enable metallb
	eval $(minikube docker-env)
elif [ $os = "Linux" ]
then
	priv="sudo"
	$priv apt-get update
	mandatory="conntrack minikube openssl lftp filezilla"
	for prog in $mandatory
	do
		if [ ! $(command -v $prog) ]
		then
			$priv apt-get install -y $prog
		fi
		if [ $prog = "minikube" ]
		then
			$priv minikube delete
		fi
	done
	$priv chmod -x /usr/bin/docker-credential-secretservice
	$priv mv /home/user42/.kube /home/user42/.minikube $HOME
	$priv chown -R $USER $HOME/.kube $HOME/.minikube
	$priv minikube start --driver=none
	$priv kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.3/manifests/namespace.yaml
	$priv kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.3/manifests/metallb.yaml
	$priv kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
fi
servicesList="mysql influxdb telegraf vsftpd nginx wordpress phpmyadmin grafana"
for image in $servicesList
do
	$priv docker build -f ./srcs/$image/Dockerfile . --tag $image
done
$priv kubectl apply -f ./srcs/Common.yaml
for service in $servicesList
do
	$priv kubectl apply -f ./srcs/$service/Setup.yaml
done
unset os priv mandatory prog image servicesList
printf "\n### LOGINS ###\n"
printf " Administrator access \t\t= admin:root\n Ssh and ftps access \t\t= jobenass:jobenass (by default)\n Users access (wordpress)\t= Mae/Keran/Tessa/Sanjay:0123\n\n"
printf "### URLS ###\n"
printf " Nginx access \t\t= http://192.168.99.101:80 ssh:22\n Wordpress access \t= http://192.168.99.102:5050\n Phpmyadmin access \t= http://192.168.99.103:5000\n Grafana access \t= http://192.168.99.104:3000\n Ftps access \t\t= ftp://192.168.99.100:21\n"

# Delete old key connexion ==>		ssh-keygen -f "/home/$USER/.ssh/known_hosts" -R "192.168.99.101"
# Disable error self-signed certificate in lftp ==>		set ssl:verify-certificate no